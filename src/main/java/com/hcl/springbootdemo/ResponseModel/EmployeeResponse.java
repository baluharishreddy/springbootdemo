package com.hcl.springbootdemo.ResponseModel;

import com.hcl.springbootdemo.entity.Employee;

public class EmployeeResponse
{
	
	private Employee employee;
	
	private String message;
	
	private String statusMessage;
	
	private String StatusCode;

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}
	
	

}
