package com.hcl.springbootdemo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.springbootdemo.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>
{
	
	Employee findById(int id);
	
	public List<Employee> findByName(String name);
	
	@Query("SELECT e FROM Employee e WHERE e.name = :name")
	public List<Employee> getEmployees(@Param("name") String name);

}
