package com.hcl.springbootdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.springbootdemo.entity.Employee;
import com.hcl.springbootdemo.repository.EmployeeRepository;

@Service
public class EmployeeService 
{
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	public Employee getEmployee(int id)
	{
		return employeeRepository.findById(id);
	}
	
	public Employee saveEmployee(Employee employee)
	{
		return employeeRepository.save(employee);
	}
	
	public void deleteEmployee(int id)
	{
		Employee employee = new Employee();
		employee.setId(id);
		employeeRepository.delete(employee);
	}
	
	public List<Employee> getAll()
	{
		return employeeRepository.findAll();
	}
	
	public List<Employee> getAllByName(String name)
	{
		return employeeRepository.getEmployees(name);
	}
	
}
