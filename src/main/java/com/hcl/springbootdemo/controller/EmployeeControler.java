package com.hcl.springbootdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.springbootdemo.ResponseModel.EmployeeResponse;
import com.hcl.springbootdemo.entity.Employee;
import com.hcl.springbootdemo.service.EmployeeService;

@RestController
public class EmployeeControler
{
	
	@Autowired
	EmployeeService employeeService;
	
	@GetMapping(value="/employee/{id}")
	public Employee getEmployeeById(@PathVariable int id)
	{
		System.out.println("id === "+id);
		return employeeService.getEmployee(id);
	}
	
	@PostMapping(value="/employee/save")
	public EmployeeResponse saveEmployee(@RequestBody Employee employee)
	{
		Employee resEmployee = employeeService.saveEmployee(employee);
		EmployeeResponse employeeResponse = new EmployeeResponse();
		employeeResponse.setEmployee(resEmployee);
		employeeResponse.setMessage("Success");
		employeeResponse.setStatusCode("200");
		employeeResponse.setStatusMessage("OK");
		return employeeResponse;
	}
	
	@DeleteMapping(value="/employee/delete/{id}")
	public String deleteEmployee(@PathVariable int id)
	{
		employeeService.deleteEmployee(id);
		return "success";
	}
	
	@GetMapping(value="/employee/all")
	public List<Employee> getAll()
	{
		return employeeService.getAll();
	}
	
	@GetMapping(value="/employee/get")
	public List<Employee> getAllByName(@RequestParam String name)
	{
		return employeeService.getAllByName(name);
	}
	
	
	
	
}
